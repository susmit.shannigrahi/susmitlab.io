+++
# About/Biography widget.
widget = "about"
active = true
date = "2016-04-20T00:00:00"

# Order that this section will appear in.
weight = 5

# List your academic interests.
[interests]
  interests = [
    "Computer Networks",
    "Future Internet Architectures",
    "Networking IoT Devices",
    "Future Mobile Communications"
  ]

# List your qualifications (such as academic degrees).
[[education.courses]]
  course = "Ph.D. in Computer Science"
  institution = "Colorado State University"
#  year =  2018

[[education.courses]]
  course = "Masters in Computer Science"
  institution = "Colorado State University"
  year = 2013

[[education.courses]]
  course = "Masters in Computer Science"
  institution = "Jadavpur University"
  year = 2009

[[education.courses]]
  course = "Bachelor in Computer Science"
  institution = "West Bengal University of Technology"
  year = 2007

+++

I am a Ph.D. candidate in the [Network Security Group](www.netsec.colostate.edu) at Colorado State University.

My research focus is to design next-generation data management protocols for scientific communities that have a large amount of data.
I design, deploy, and study scientific data management frameworks in collaboration with domain scientists.
I use the deployments to drive research that helps to mitigate scientific data management problems.
I am also interested in networking protocols for IoT devices and future Mobile networks.

I expect to finish my Ph.D. in Spring 2018. So far, I have focused on using Named Data Networking ([NDN](www.named-data.net)), a future networking architecture,
for large scientific data management. With the help of my colleagues, I have developed [ndn-sci](http://atmos-sac.es.net), a prototype application for managing Climate as High Energy Particle Physics data. I also contribute to the NDN codebase, libraries, and various applications.

I received my Masters in Computer Science from Colorado State University in 2013. I also received  Masters in Computer Science from Jadavpur University, India in 2009. I received B.Tech in Computer Science from West Bengal University of Technology, India in 2007.


